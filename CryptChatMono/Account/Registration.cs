﻿using System;
using System.Collections.Generic;
using CryptChatMono.Security.Psec;
using CryptChatMono.Security.Ksec;
using CryptChatMono.API.Handler;
namespace CryptChatMono.Account.Registration
{
    public static class Registration
    {
        public static bool register(string path, string username, string password)
        {
            string salt = PasswordHash.CreateSalt();
            string hash = PasswordHash.HashPassword(password, salt);
            string[] keys = RSA.Generate();
            string pubkey = keys[1];
            string privkey = keys[0];
            if (Handler.Register(username, hash, salt, pubkey))
            {
                Dictionary<string, string> data = Handler.GetUser(username);
                RSA.SavePrivate(password, salt, privkey, path + $"{data["_id"]}");
                return true;
            }
            return false;
        }
    }
}
