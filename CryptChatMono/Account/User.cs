﻿using System;
using System.Collections.Generic;

namespace CryptChatMono.Account.User
{
    public class User
    {
        public string _id;
        public string hash;
        public string pubkey;
        public string username;
        public string salt;
        private readonly string password;
        private string privkey;

        public User(string _id, string hash, string pubkey, string username, string salt, string password)
        {
            this._id = _id;
            this.hash = hash;
            this.pubkey = pubkey;
            this.username = username;
            this.salt = salt;
            this.password = password;
        }
        public User(Dictionary<string, string> data, string password)
        {
            this._id = data["_id"];
            this.hash = data["hash"];
            this.pubkey = data["public"];
            this.username = data["username"];
            this.salt = data["salt"];
            this.password = password;
        }

        public User(string _id, string hash, string pubkey, string username, string salt, string password, string privkey)
        {
            this._id = _id;
            this.hash = hash;
            this.pubkey = pubkey;
            this.username = username;
            this.salt = salt;
            this.password = password;
            this.privkey = privkey;
        }
        public User(Dictionary<string, string> data, string password, string privkey)
        {
            this._id = data["_id"];
            this.hash = data["hash"];
            this.pubkey = data["public"];
            this.username = data["username"];
            this.salt = data["salt"];
            this.password = password;
            this.privkey = privkey;
        }

        public void get_private_from_file(string path)
        {
            privkey = Security.Ksec.RSA.ReadPrivate(password, salt, path);
        }

        public string get__id()
        {
            return _id;
        }
        public string get_hash()
        {
            return hash;
        }
        public string get_pubkey()
        {
            return pubkey;
        }
        public string get_username()
        {
            return username;
        }
        public string get_salt()
        {
            return salt;
        }
        public string get_privkey()
        {
            return privkey;
        }
    }
}
