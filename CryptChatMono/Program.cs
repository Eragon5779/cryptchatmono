﻿using System;
using Gtk;
using System.IO;
using CryptChatMono.Account.User;

namespace CryptChatMono
{
    class MainClass
    {
        public static User self;
        public static readonly string FILEPATH = Environment.GetEnvironmentVariable("HOME") + "/.cryptchat/";
        public static void Main(string[] args)
        {
            if (!Directory.Exists(FILEPATH))
            {
                Directory.CreateDirectory(FILEPATH);
            }

            Application.Init();
            MainWindow win = new MainWindow();
            win.Show();
            Application.Run();

        }
    }
}
