﻿using System;
using System.Collections.Generic;
using Gtk;
using CryptChatMono.Account.Registration;
using CryptChatMono.Account.User;
using CryptChatMono.API.Handler;
using CryptChatMono.Security.Psec;
using CryptChatMono.Security.Ksec;
using CryptChatMono.Security.Msec;
using CryptChatMono;

public partial class MainWindow : Gtk.Window
{
    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }

    protected void btnLogin_Clicked(object sender, EventArgs e)
    {
        string username = txtUser.Text;
        string password = txtPass.Text;
        string salt = Handler.GetSalt(username);
        string hash = PasswordHash.HashPassword(password, salt);
        if (Handler.Login(username, hash))
        {
            Dictionary<string, string> data = Handler.GetUser(username);
            MainClass.self = new User(data, password, RSA.ReadPrivate(password, data["salt"], MainClass.FILEPATH + $"{data["_id"]}"));
        }
        else
        {
            ShowErrorMessage(this, "Authentication Error", "Failed to log in!\nPlease check that all info is correct");
        }
    }

    void ShowErrorMessage(Window parent, string title, string message)
    {
        MessageDialog m = new MessageDialog(parent, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, message)
        {
            Title = title
        };
        if ((ResponseType)m.Run() == ResponseType.Ok)
        {
            m.Destroy();
        }
    }

    void ShowSuccessMessage(Window parent, string title, string message)
    {
        MessageDialog m = new MessageDialog(parent, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, message)
        {
            Title = title
        };
        if ((ResponseType)m.Run() == ResponseType.Ok)
        {
            m.Destroy();
        }
    }

    protected void btnRegister_Clicked(object sender, EventArgs e)
    {
        string username = txtUser.Text;
        string password = txtPass.Text;
        if (Registration.register(MainClass.FILEPATH, username, password))
        {
            Dictionary<string, string> data = Handler.GetUser(username);
            MainClass.self = new User(data, password, RSA.ReadPrivate(password, data["salt"], MainClass.FILEPATH + $"{data["_id"]}"));
            ShowSuccessMessage(this, "Registration Success", $"Registered successfully!\nWelcome to CryptChat {username}.");
        }
        else
        {
            ShowErrorMessage(this, "Registration Failure", "Failed to register!\nIf this issue continues, please try a\ndifferent username.");
        }
    }
}
