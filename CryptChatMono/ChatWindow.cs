﻿using System;
using Gtk;
using CryptChatMono.Security.Msec;
using CryptChatMono.Security.Ksec;
using CryptChatMono.API.Handler;
using System.Security.Cryptography;
namespace CryptChatMono
{
    public partial class ChatWindow : Gtk.Window
    {
        public ChatWindow() :
                base(Gtk.WindowType.Toplevel)
        {
            this.Build();
        }

        protected void OnDeleteEvent(object sender, DeleteEventArgs a)
        {
            Application.Quit();
            a.RetVal = true;
        }

        protected void btnSend_Clicked(object sender, EventArgs e)
        {
            /// <TODO>
            /// Add logic to have username loaded from current chat
            /// Send message
            /// </TODO>
            string message = txtMessage.Text;
            RSAParameters priv = Security.Ksec.RSA.FromString(MainClass.self.get_privkey(), true);
            //RSAParameters pub = RSA.FromString(Handler.GetUser(chat.username)); //Uncomment when chats window figured out
            string enc = Msec.Encrypt(message, priv);
            string sig = Msec.Sign(message, priv);

        }
    }
}
