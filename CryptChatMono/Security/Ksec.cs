﻿using System;
using System.Text;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using CryptChatMono.Security.Psec;
namespace CryptChatMono.Security.Ksec
{
    public class RSA
    {
        public static string[] Generate()
        {
            var csp = new RSACryptoServiceProvider(4096);
            var privkey = csp.ExportParameters(true);
            var pubkey = csp.ExportParameters(false);

            string pubkeystring;
            {
                var sw = new StringWriter();
                var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
                xs.Serialize(sw, pubkey);
                pubkeystring = sw.ToString();
            }
            string privkeystring;
            {
                var sw = new System.IO.StringWriter();
                var xs = new System.Xml.Serialization.XmlSerializer(typeof(RSAParameters));
                xs.Serialize(sw, privkey);
                privkeystring = sw.ToString();
            }
            return new string[2] { privkeystring, pubkeystring };
        }

        public static RSAParameters FromString(string key, bool priv)
        {
            RSAParameters keyparams;
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.FromXmlString(key);
                keyparams = rsa.ExportParameters(priv);
            }
            return keyparams;
        }

        public static string ReadPrivate(string password, string salt, string path)
        {
            string enc = File.ReadAllText(path + ".pem");
            return AES.Decrypt(enc, password, salt);
        }

        public static void SavePrivate(string password, string salt, string privkey, string path)
        {
            string enc = AES.Encrypt(privkey, password, salt);
            File.WriteAllText(path + ".pem", enc);
        }
    }

    public class AES
    {

        public static string Encrypt(string message, string password, string salt,
                                                    int iters = 100000, int keysize = 256)
        {
            if (String.IsNullOrEmpty(message))
            {
                return "";
            }

            byte[] vectorbytes = new byte[16];
            byte[] saltbytes = Encoding.ASCII.GetBytes(salt);
            using (SHA512 sha512 = new SHA512Managed())
            {
                vectorbytes = sha512.ComputeHash(saltbytes).Take(16).ToArray();
            }
            byte[] keybytes = PasswordHash.GetPBKDF2Bytes(password, saltbytes, iters, keysize / 8);
            byte[] cipherbytes;
            using (RijndaelManaged aes = new RijndaelManaged())
            {
                aes.IV = vectorbytes;
                aes.Key = keybytes;
                ICryptoTransform enc = aes.CreateEncryptor(aes.Key, aes.IV);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, enc, CryptoStreamMode.Write))
                    {
                        using (StreamWriter sw = new StreamWriter(cs))
                        {
                            sw.Write(message);
                        }
                        cipherbytes = ms.ToArray();
                    }
                }
            }
            return Convert.ToBase64String(cipherbytes);
        }

        public static string Decrypt(string ciphertext, string password, string salt,
                                                    int iters = 100000, int keysize = 256)
        {
            if (String.IsNullOrEmpty(ciphertext))
            {
                return "";
            }
            byte[] vectorbytes = new byte[16];
            byte[] saltbytes = Encoding.ASCII.GetBytes(salt);
            using (SHA512 sha512 = new SHA512Managed())
            {
                vectorbytes = sha512.ComputeHash(saltbytes).Take(16).ToArray();
            }
            byte[] cipherbytes = Convert.FromBase64String(ciphertext);
            byte[] keybytes = PasswordHash.GetPBKDF2Bytes(password, saltbytes, iters, keysize / 8);
            string message;
            using (RijndaelManaged aes = new RijndaelManaged())
            {
                aes.Key = keybytes;
                aes.IV = vectorbytes;
                ICryptoTransform dec = aes.CreateDecryptor(aes.Key, aes.IV);
                using (MemoryStream ms = new MemoryStream(cipherbytes))
                {
                    using (CryptoStream cs = new CryptoStream(ms, dec, CryptoStreamMode.Read))
                    {
                        using (StreamReader sr = new StreamReader(cs))
                        {
                            message = sr.ReadToEnd();
                        }
                    }
                }
            }
            return message;
        }
    }
}
