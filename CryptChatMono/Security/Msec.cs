﻿using System;
using System.Text;
using System.Security.Cryptography;
namespace CryptChatMono.Security.Msec
{
    public class Msec
    {
        public static string Encrypt(string message, RSAParameters rec_public)
        {
            byte[] encrypted;
            using (var rsa = new RSACryptoServiceProvider())
            {
                try
                {
                    rsa.ImportParameters(rec_public);
                    encrypted = rsa.Encrypt(Encoding.ASCII.GetBytes(message), true);
                }
                catch (CryptographicException e)
                {
                    Console.WriteLine(e.Message);
                    return null;
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
            return Convert.ToBase64String(encrypted);
            
        }

        public static string Decrypt(string enc, RSAParameters rec_private)
        {
            byte[] message;
            using (var rsa = new RSACryptoServiceProvider())
            {
                try
                {
                    rsa.ImportParameters(rec_private);
                    message = rsa.Decrypt(Convert.FromBase64String(enc), true);
                }
                catch (CryptographicException e)
                {
                    Console.WriteLine(e.Message);
                    return null;
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
            return Encoding.ASCII.GetString(message);
        }

        public static bool Verify(string message, string signature, RSAParameters pubkey)
        {
            bool verified = false;
            using (var rsa = new RSACryptoServiceProvider())
            {
                byte[] todo = Encoding.ASCII.GetBytes(message);
                byte[] signed = Convert.FromBase64String(signature);
                try
                {
                    rsa.ImportParameters(pubkey);
                    SHA512Managed sha = new SHA512Managed();
                    byte[] hash = sha.ComputeHash(signed);
                    verified = rsa.VerifyData(todo, CryptoConfig.MapNameToOID("SHA512"), signed);
                }
                catch (CryptographicException e)
                {
                    Console.WriteLine(e.Message);
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }

            return verified;
        }

        public static string Sign(string message, RSAParameters privkey)
        {
            byte[] signedbytes;
            using (var rsa = new RSACryptoServiceProvider())
            {
                byte[] orig = Encoding.ASCII.GetBytes(message);

                try
                {
                    rsa.ImportParameters(privkey);
                    signedbytes = rsa.SignData(orig, CryptoConfig.MapNameToOID("SHA512"));
                }
                catch (CryptographicException e)
                {
                    Console.WriteLine(e.Message);
                    return null;
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
            return Convert.ToBase64String(signedbytes);
        }
    }
}
