﻿using System;
using System.Net;
using System.Text;
using System.Collections.Generic;
using System.Collections.Specialized;
using Newtonsoft.Json;

namespace CryptChatMono.API.Handler
{
    public static class Handler
    {
        private static readonly string address = 'http://104.248.226.211:5000';
        #region utility
        public static string Fix_Public(string pub)
        {
            return $"<?xml version=\"1.0\" encoding=\"utf-16\"?>\n<RSAParameters xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n  <Exponent>AQAB</Exponent>\n  <Modulus>{pub}</Modulus>\n</RSAParameters>";
        }
        public static string Strip_Public(string pub)
        {
            return pub.Split('>')[5].Split('<')[0];
        }
        public static Dictionary<string, string> ParseRawJson(string json)
        {
            return JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
        }
        #endregion
        #region auth
        public static string GetSalt(string username)
        {
            var json = "";
            using (WebClient wc = new WebClient())
            {
                var post = new NameValueCollection
                {
                    ["username"] = username
                };
                var response = wc.UploadValues($"{address}/auth/getsalt", post);
                json = Encoding.Default.GetString(response);
            }
            Dictionary<string, string> data = ParseRawJson(json);
            return data["salt"];
        }

        public static Dictionary<string, string> GetUser(string username)
        {
            var json = "";
            using (WebClient wc = new WebClient())
            {
                var post = new NameValueCollection
                {
                    ["username"] = username
                };
                var response = wc.UploadValues($"{address}/auth/getuser", post);
                json = Encoding.Default.GetString(response);
            }
            Dictionary<string, string> data = ParseRawJson(json);
            data["public"] = Fix_Public(data["public"]);
            return data;
        }

        public static bool Login(string username, string hash)
        {
            var json = "";
            using (WebClient wc = new WebClient())
            {
                var post = new NameValueCollection
                {
                    ["username"] = username,
                    ["hash"] = hash
                };
                var response = wc.UploadValues($"{address}/auth/login", post);
                json = Encoding.Default.GetString(response);
            }
            Dictionary<string, string> data = ParseRawJson(json);
            return data["login"] == "success";
        }

        public static bool Register(string username, string hash, string salt, string pubkey)
        {
            var json = "";
            using (WebClient wc = new WebClient())
            {
                var post = new NameValueCollection
                {
                    ["username"] = username,
                    ["hash"] = hash,
                    ["salt"] = salt,
                    ["public"] = Strip_Public(pubkey)
                };
                var response = wc.UploadValues($"{address}/auth/register", post);
                json = Encoding.Default.GetString(response);
            }
            Dictionary<string, string> data = ParseRawJson(json);
             return data["registration"] == "success";
        }
        #endregion
        #region user
        public static string Get_Public(string username)
        {
            var json = "";
            using (WebClient wc = new WebClient())
            {
                var post = new NameValueCollection
                {
                    ["username"] = username
                };
                var response = wc.UploadValues($"{address}/user/getuser", post);
                json = Encoding.Default.GetString(response);
            }
            Dictionary<string, string> data = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
            return Fix_Public(data["public"]);
        }
        #endregion
        #region msg
        public static bool Send_Message(string sender, string recipient, string message, string signature)
        {
            var json = "";
            using (WebClient wc = new WebClient())
            {
                var post = new NameValueCollection
                {
                    ["sender"] = sender,
                    ["recipient"] = recipient,
                    ["message"] = message,
                    ["signature"] = signature
                };
                var response = wc.UploadValues($"{address}/msg/sendmsg", post);
                json = Encoding.Default.GetString(response);
            }
            Dictionary<string, string> data = ParseRawJson(json);
            return data["send"] == "success";
        }

        public static Dictionary<string, string> Get_Message(string id)
        {
            var json = "";
            using (WebClient wc = new WebClient())
            {
                var post = new NameValueCollection
                {
                    ["_id"] =  id
                };
                var response = wc.UploadValues($"{address}/msg/getmsg", post);
                json = Encoding.Default.GetString(response);
            }
            Dictionary<string, string> data = ParseRawJson(json);
            return data;
        }
        #endregion
        #region chat
        public static Dictionary<string, string> Get_Chat(string id)
        {
            var json = "";
            using (WebClient wc = new WebClient())
            {
                var post = new NameValueCollection
                {
                    ["_id"] = id
                };
                var response = wc.UploadValues($"{address}/chat/getchat", post);
                json = Encoding.Default.GetString(response);
            }
            Dictionary<string, string> data = ParseRawJson(json);
            return data;
        }
        public static Dictionary<string, string> Get_New(string id, string oldest)
        {
            var json = "";
            using (WebClient wc = new WebClient())
            {
                var post = new NameValueCollection
                {
                    ["chat_id"] = id,
                    ["oldest"] = oldest
                };
                var response = wc.UploadValues($"{address}/chat/getnew", post);
                json = Encoding.Default.GetString(response);
            }
            Dictionary<string, string> data = ParseRawJson(json);
            return data;
        }

        #endregion
    }
}
